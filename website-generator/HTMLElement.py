class parentElement:
    def __init__(self, elements):
        self.elements = elements

    def prep_str(self):
        self.inner = ''
        for self.element in self.elements:
            self.inner += f'{self.element}\n'
        return self.inner

class childElement:
    def __init__(self, text=None, attributes=None):
        self.text = text
        self.attributes = attributes

    def prep_str(self):
        self.formatted_attrs = ''
        classes = ''
        if self.attributes != None:
            for self.attribute in self.attributes:
                if self.attribute == 'class':
                    for css_class in self.attributes[self.attribute]:
                        classes += f'{css_class} '
                    self.formatted_attrs += f'{self.attribute}="{classes}" '
                else:
                    self.formatted_attrs += f'{self.attribute}="{self.attributes[self.attribute]}" '
        return [self.formatted_attrs, self.text]

class html(parentElement):
    def __str__(self):
        inner = super().prep_str()
        return f'<html>{inner}</html>'

class head(parentElement):
    def __str__(self):
        inner = super().prep_str()
        return f'<head>{inner}</head>'

class body(parentElement):
    def __str__(self):
        inner = super().prep_str()
        return f'<body>{inner}</body>'

class h1(childElement):
    def __str__(self):
        self.prepped_strings = super().prep_str()
        return f'<h1 {self.prepped_strings[0]}>{self.prepped_strings[1]}</h1>'.replace(' >', '>')

class title(childElement):
    def __str__(self):
        self.prepped_strings = super().prep_str()
        return f'<title {self.prepped_strings[0]}>{self.prepped_strings[1]}</title>'.replace(' >', '>')







